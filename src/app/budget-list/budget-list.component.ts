import { Component, OnInit, OnDestroy } from '@angular/core';
import { Store } from '@angular/expense-tracker/expense-tracker/node_modules/@ngrx/store';
import { Observable, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { Budget } from '../budget.model';
import * as BudgetActions from '../store/budget.actions';

@Component({
  selector: 'app-budget-list',
  templateUrl: './budget-list.component.html',
  styleUrls: ['./budget-list.component.scss']
})
export class BudgetListComponent implements OnInit, OnDestroy {
  readonly budget$: Observable<{
    budget: Budget[], income: [], expenses: [], incomeTotal: number, expensesTotal: number
  }> = this.store.select('budgetList');
  private subBudget: Subscription = new Subscription();
  public income: [];
  public expenses: [];
  public incomeTotal: string;
  public expensesTotal: string;
  public amount: number = null;
  public desc: string = null;
  public amountExp: number = null;
  public descExp: string = null;
  public errorMessageIncome: string = null;
  public errorMessageExpenses: string = null;

  constructor(private store: Store<{ budgetList }>) {
    this.subBudget = this.budget$
      .pipe(
        map(budget => {
          this.income = budget.income;
          this.expenses = budget.expenses;
          const incomeTotalNumber = budget.incomeTotal;
          this.incomeTotal = incomeTotalNumber.toFixed(2);
          const expensesTotalNumber = budget.expensesTotal;
          this.expensesTotal = expensesTotalNumber.toFixed(2);
        })
      ).subscribe();
  }

  validateInput(amount) {
    const regExp = new RegExp(/^\d+(?:\.\d{1,2})?$/);
    if (regExp.test(amount)) {
      return true;
    } else {
      return false;
    }
  }

  newIncomeItem(desc, amount) {
    const check = this.validateInput(amount);
    if (check && desc) {
      const amountFloat = parseFloat(amount);
      const newIncome = new Budget(desc, amountFloat);
      this.store.dispatch(new BudgetActions.AddIncome(newIncome));
      this.amount = null;
      this.desc = null;
      this.errorMessageIncome = null;
    } else {
      if (!desc) {
        this.errorMessageIncome = 'Please fill in a description.';
      } else {
        this.errorMessageIncome = 'Only use numbers and a maximum of 2 decimals.';
      }
    }
  }

  newExpensesItem(descExp, amountExp) {
    const check = this.validateInput(amountExp);
    if (check && descExp) {
      const amountFloat = parseFloat(amountExp);
      const newExpenses = new Budget(descExp, amountFloat);
      this.store.dispatch(new BudgetActions.AddExpenses(newExpenses));
      this.amountExp = null;
      this.descExp = null;
      this.errorMessageExpenses = null;
    } else {
      if (!descExp) {
        this.errorMessageExpenses = 'Please fill in a description.';
      } else {
        this.errorMessageExpenses = 'Only use numbers and a maximum of 2 decimals.';
      }
    }
  }

  removeIncome(id, amount) {
    const amountFloat = parseFloat(amount);
    this.store.dispatch(new BudgetActions.RemoveIncome({id, amount: amountFloat}));
  }

  removeExpenses(id, amount) {
    const amountFloat = parseFloat(amount);
    this.store.dispatch(new BudgetActions.RemoveExpenses({id, amount: amountFloat}));
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.subBudget.unsubscribe();
  }

}
