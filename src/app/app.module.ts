import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from '../environments/environment';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { BudgetListComponent } from './budget-list/budget-list.component';
import { BudgetReducer } from './store/budget.reducer';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    BudgetListComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    StoreModule.forRoot({budgetList: BudgetReducer}),
    StoreDevtoolsModule.instrument({
      maxAge: 25,
      logOnly: environment.production,
    }),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
