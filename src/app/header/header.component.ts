import { Component, OnInit, OnDestroy } from '@angular/core';
import { Store } from '@angular/expense-tracker/expense-tracker/node_modules/@ngrx/store';
import { Observable, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { saveAs } from 'file-saver';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, OnDestroy {
  readonly budget$: Observable<{ income: [], expenses: [], incomeTotal: number, expensesTotal: number }> = this.store.select('budgetList');
  private subBudget: Subscription = new Subscription();
  private totalNumber: number;
  public total: string;
  public isPositive: boolean;
  private currentDate: number = new Date().getMonth();
  private monthsArr: string[] = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December',
  ];
  private income: [];
  private expenses: [];
  private incomeTotal: number;
  private expensesTotal: number;
  private exportData: {};
  public month: string = this.monthsArr[this.currentDate];

  constructor(private store: Store<{ budgetList }>) {
    this.subBudget = this.budget$
      .pipe(
        map(budget => {
          this.income = budget.income;
          this.expenses = budget.expenses;
          this.incomeTotal = budget.incomeTotal;
          this.expensesTotal = budget.expensesTotal;
          this.totalNumber = budget.incomeTotal - budget.expensesTotal;
          this.totalNumber = Math.round(this.totalNumber * 100) / 100;
          this.total = this.totalNumber.toFixed(2);
          if (this.totalNumber >= 0) {
            this.isPositive = true;
          } else if (this.totalNumber < 0) {
            this.isPositive = false;
          }
        })
      ).subscribe();
  }

  export() {
    this.exportData = {
      month: this.month,
      total: this.total,
      incomeTotal: this.incomeTotal,
      income: this.income,
      expensesTotal: this.expensesTotal,
      expenses: this.expenses
    };
    const json = JSON.stringify(this.exportData);
    const filename = `${this.month} expense tracker.txt`;
    const blob = new Blob([json], { type: 'octet/stream' });
    saveAs(blob, filename);
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.subBudget.unsubscribe();
  }
}
