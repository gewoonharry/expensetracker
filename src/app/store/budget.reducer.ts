import * as BudgetActions from './budget.actions';

const initialState = {
  income: [],
  incomeTotal: 0,
  expenses: [],
  expensesTotal: 0
};

function roundNumber(r: number) {
  return Math.round(r * 100) / 100;
}

export function BudgetReducer(
  state = initialState,
  action: BudgetActions.BudgetActions) {
  switch (action.type) {
    case BudgetActions.ADD_INCOME:
      let incomeCalc: number = state.incomeTotal + action.payload.amount;
      incomeCalc = roundNumber(incomeCalc);
      return {
        ...state,
        income: [...state.income, action.payload],
        incomeTotal: incomeCalc
      };
    case BudgetActions.ADD_EXPENSES:
      let expensesCalc: number = state.expensesTotal + action.payload.amount;
      expensesCalc = roundNumber(expensesCalc);
      return {
        ...state,
        expenses: [...state.expenses, action.payload],
        expensesTotal: expensesCalc
      };
    case BudgetActions.REMOVE_INCOME:
      let removeIncomeCalc: number = state.incomeTotal - action.payload.amount;
      removeIncomeCalc = roundNumber(removeIncomeCalc);
      return {
        ...state,
        income: state.income.filter((income, incomeIndex) => {
          return incomeIndex !== action.payload.id;
        }),
        incomeTotal: removeIncomeCalc
      };
    case BudgetActions.REMOVE_EXPENSES:
      let removeExpensesCalc: number = state.expensesTotal - action.payload.amount;
      removeExpensesCalc = roundNumber(removeExpensesCalc);
      return {
        ...state,
        expenses: state.expenses.filter((expenses, expensesIndex) => {
          return expensesIndex !== action.payload.id;
        }),
        expensesTotal: removeExpensesCalc
      };
    default:
      return state;
  }
}
