import { Action } from '@ngrx/store';
import { Budget } from '../budget.model';

export const ADD_INCOME = '[BUDGET-LIST]ADD_INCOME';
export const ADD_EXPENSES = '[BUDGET-LIST]ADD_EXPENSES';
export const REMOVE_INCOME = '[BUDGET-LIST]REMOVE_INCOME';
export const REMOVE_EXPENSES = '[BUDGET-LIST]REMOVE_EXPENSES';

export class AddIncome implements Action {
  readonly type = ADD_INCOME;

  constructor(public payload: Budget) {}
}

export class AddExpenses implements Action {
  readonly type = ADD_EXPENSES;

  constructor(public payload: Budget) {}
}

export class RemoveIncome implements Action {
  readonly type = REMOVE_INCOME;

  constructor(public payload: {
    id: number,
    amount: number
  }) {}
}

export class RemoveExpenses implements Action {
  readonly type = REMOVE_EXPENSES;

  constructor(public payload: {
    id: number,
    amount: number
  }) {}
}

export type BudgetActions =
  | AddIncome
  | AddExpenses
  | RemoveIncome
  | RemoveExpenses;
